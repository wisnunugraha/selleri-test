const { response } = require("express");
let FilterRespository = require("../Repository/FilterReposiroty");
module.exports = {
  async listUser(req, res)
  {
    let FilterRepo = new FilterRespository;
    let data = await FilterRepo.ListDataFormat();
    return res.send(data);
  },

  async listByShort(req, res)
  {
    console.log(req.body)
    const type = parseInt(req.body.type);
    const min = parseInt(req.body.min);
    const max = parseInt(req.body.max);
    let FilterRepo = new FilterRespository;
    if(type === 1)
    {
      // Mr
      let value = "Mr";
      let data = await FilterRepo.ListDataSortTitle(value);
      return res.send(data);
    }
    else if(type === 2)
    {
      // Mrs
      let value = "Mrs";
      let data = await FilterRepo.ListDataSortTitle(value);
      return res.send(data);
    }
    else if(type === 3)
    {
      // Ms
      let value = "Ms";
      let data = await FilterRepo.ListDataSortTitle(value);
      return res.send(data);
    }
    else if(type === 4)
    {
      // Miss
      let value = "Miss";
      let data = await FilterRepo.ListDataSortTitle(value);
      return res.send(data);
    }
    else if(type === 5)
    {
      // Madame
      let value = "Madame";
      let data = await FilterRepo.ListDataSortTitle(value);
      return res.send(data);
    }
    else if(type === 6)
    {
      // Monsieur
      let value = "Monsieur";
      let data = await FilterRepo.ListDataSortTitle(value);
      return res.send(data);
    }
    else if(type === 7)
    {
      // Sort ASC
      let data = await FilterRepo.ListDataAgeAsc();
      return res.send(data);
    }
    else if(type === 8)
    {
      // Min Max
      if(!min && !max )
      {
        let response = {
          status : "failed",
          code   : 202,
          message: "Sorry MIN & MAX cannot empty",
          type : {
            1: "Mr",
            2: "Mrs",
            3: "Ms",
            4: "Miss",
            5: "Madame",
            6: "Monsier",
            7: "Sort Age ASC",
            8: "Min Max Age",
          } 
        }
        return res.send(response);
      }
      let data = await FilterRepo.ListDataAgeMinMax(min,max);
      return res.send(data);
    }
    else
    {
      let response = {
        status : "failed",
        code   : 201,
        message: "Please insert type",
        type : {
          1: "Mr",
          2: "Mrs",
          3: "Ms",
          4: "Miss",
          5: "Madame",
          6: "Monsier",
          7: "Sort Age ASC",
          8: "Min Max Age",
        } 
      }
      return res.send(response);
    }

  }
};