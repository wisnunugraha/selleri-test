let CombinationRepo =  require("../Repository/CombinationRepository");
module.exports = {

  async listData(req, res){
    let CombRepo = new CombinationRepo;

    let listValue =  await CombRepo.listData();
    return res.send(listValue);
  },

}