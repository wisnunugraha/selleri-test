
const CombController = require("../Controller/CombinationController");
const FilterController = require("../Controller/FilterController");
module.exports = function (app) {
  // Define URi Of Version
  const url = '/assignment/api-v1/'
  // Routes Combintaion Here
  app.get( url +'combintaion-list', CombController.listData);

  // Routes Filter Here
  app.post(url + "filter-list", FilterController.listUser);
  app.post(url + "filter-sort", FilterController.listByShort);
}
