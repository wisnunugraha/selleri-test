const axios = require('axios');

class FilterRespository {
   getUser() {
    let data = axios.get('https://randomuser.me/api/?page=1&results=20');
    return data;
  }

  async ListDataFormat() {
    let dataGet = await this.getUser();
    let dataValue = [];
    let dataList = dataGet.data.results;
    for(let i =0; i < dataList.length; i++)
    {
     let setValue = {
        fullname  :  dataList[i].name.title +". "+ dataList[i].name.first +" "+ dataList[i].name.last,
        age       :  dataList[i].registered.age,
        city      :  dataList[i].location.city,
        email     :  dataList[i].email,
        phone     :  dataList[i].phone,
        picture   :  dataList[i].picture.large
      }
      dataValue.push(setValue);
    }
    let response = {
      status:"success",
      code:200,
      data:dataValue
    }
    return response;
  }

  
  async ListDataSortTitle(params) {
    let dataGet = await this.getUser();
    let dataValue = [];
    let dataList = dataGet.data.results;
    for(let i =0; i < dataList.length; i++)
    {
      // Data need only title Mrs
      if(dataList[i].name.title === params)
      {
        let setValue = {
          fullname  :  dataList[i].name.title +". "+ dataList[i].name.first +" "+ dataList[i].name.last,
          age       :  dataList[i].registered.age,
          city      :  dataList[i].location.city,
          email     :  dataList[i].email,
          phone     :  dataList[i].phone,
          picture   :  dataList[i].picture.large
        }
        dataValue.push(setValue);

      }
    }
    let response = {
      status:"success",
      code:200,
      data:dataValue
    }
    return response;
  }

  async ListDataAgeMinMax(min, max)
  {
    let dataGet = await this.getUser();
    let dataValue = [];
    let dataList = dataGet.data.results;
    for(let i =0; i < dataList.length; i++)
    {
      // Data Min Max Age
      if(dataList[i].registered.age >= min && dataList[i].registered.age <= max)
      {
        let setValue = {
          fullname  :  dataList[i].name.title +". "+ dataList[i].name.first +" "+ dataList[i].name.last,
          age       :  dataList[i].registered.age,
          city      :  dataList[i].location.city,
          email     :  dataList[i].email,
          phone     :  dataList[i].phone,
          picture   :  dataList[i].picture.large
        }
        dataValue.push(setValue);

      }
    }
    let response = {
      status:"success",
      code:200,
      data:dataValue
    }
    return response;

  }

  async ListDataAgeAsc()
  {
    let dataGet = await this.getUser();
    let dataValue = [];
    let dataList = dataGet.data.results;
    for(let i =0; i < dataList.length; i++)
    {
      // Data Asc
      let setValue = {
        fullname  :  dataList[i].name.title +". "+ dataList[i].name.first +" "+ dataList[i].name.last,
        age       :  dataList[i].registered.age,
        city      :  dataList[i].location.city,
        email     :  dataList[i].email,
        phone     :  dataList[i].phone,
        picture   :  dataList[i].picture.large
      }
      dataValue.push(setValue);
    }
      console.log(dataValue);
      dataValue.sort(function(a, b) {
        return a.age-b.age;
      });
    
    let response = {
      status:"success",
      code:200,
      data:dataValue
    }
    return response;

  }

}

module.exports = FilterRespository;