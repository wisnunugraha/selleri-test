
class CombinationRepository {

  listData() {
    const combination = ["Merah-S-Katun", "Merah-S-Polyester", "Merah-M-Katun", "Merah-M-Polyester", "Merah-L-Katun", "Merah-L-Polyester", "Merah-XL-Katun", "Merah-XL-Polyester", "Merah-XXL-Katun", "Merah-XXL-Polyester", "Kuning-S-Katun", "Kuning-S-Polyester", "Kuning-M-Katun", "Kuning-M-Polyester", "Kuning-L-Katun", "Kuning-L-Polyester", "Kuning-XL-Katun", "Kuning-XL-Polyester", "Kuning-XXL-Katun", "Kuning-XXL-Polyester", "Hijau-S-Katun", "Hijau-S-Polyester", "Hijau-M-Katun", "Hijau-M-Polyester", "Hijau-L-Katun", "Hijau-L-Polyester", "Hijau-XL-Katun", "Hijau-XL-Polyester", "Hijau-XXL-Katun", "Hijau-XXL-Polyester"]
    
    let color = [];
    let size = [];
    let material = [];
    
    for(let a in combination)
    {
      let splitValue = combination[a].split("-");
      if(color.includes(splitValue[0]) === false)
      {
        color.push(splitValue[0])
      }
      if(size.includes(splitValue[1]) === false)
      {
        size.push(splitValue[1])
      }
      if(material.includes(splitValue[2]) === false)
      {
        material.push(splitValue[2])
      }
    }
    let response = {
      status:"success",
      code:200,
      data : {
        color:color,
        size:size,
        material:material
      }
    }
    return response

  }

}

module.exports = CombinationRepository;