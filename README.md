# Selleri Test

This is assignment

## Assignment List

- [x] [Combination Array] -> URL 127.0.0.1:3000/assignment/api-v1/combintaion-list
- [x] [Filter List] -> URL 127.0.0.1:3000/assignment/api-v1/filter-list
- [x] [Sort List Such as : Title, Min & Max Age, ASC age] -> URL 127.0.0.1:3000/assignment/api-v1/sort-list

- [x] [Query Assignment] -> Open file name : QueryAssigment.txt

## Getting started

1. Clone this repository

2. npm install

3. Import PostMan  file name : API V1 Selleri - Wisnu Nugraha.postman_collection.json

4. npm start
