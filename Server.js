// Plugin Express
const express = require('express');
require('events').EventEmitter.defaultMaxListeners = 500;
const cors = require('cors')
const bodyParser = require('body-parser')

// Header
const app = express();


// Enable if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)
// see https://expressjs.com/en/guide/behind-proxies.html
app.set('trust proxy', 1);

// App Setup
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({type: "application/json"}));
app.use(bodyParser.raw());
app.use(cors());

// Put file of routes here
app.listen(3000, (err) => {
  console.log('API PORT NEW '+3000);
  require('./Routes/ServerRoutes')(app);
}) 